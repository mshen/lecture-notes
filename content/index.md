---
title: Matthew's Lecture Notes
---

# Winter 2024
- [[math-223.md|MATH 223, Linear Algebra]]

# Fall 2022
- [[comp-250.md|COMP 250, Introduction to Computer Science]]
