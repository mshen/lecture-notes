---
title: Math 223, Linear Algebra
date: 2024-04-19 EDT
description: These notes are based on lectures given by Professor Mikäel Pichot for MATH 223, Linear Algebra at McGill University during the Winter 2024 semester.
tags:
    - math
---

$$
\newcommand{\st}{\text{ s.t. }}
\newcommand{\span}{\text{span}}
\newcommand{\dim}{\text{dim}}
\newcommand{\ker}{\text{ker}}
\newcommand{\rk}{\text{rank}}
\newcommand{\Im}{\text{Im}}
\newcommand{\Id}{\text{Id}}
\newcommand{\det}{\text{det}}
\newcommand{\ebasis}{e_1, \ldots, e_n}
\newcommand{\veckn}{\begin{pmatrix}x_1 \\ \vdots \\ x_n\end{pmatrix}}
$$
These notes are based on lectures given by Professor Mikäel Pichot for MATH 223, Linear Algebra at McGill University during the Winter 2024 semester.

# Chapter 1: Vectors in $\mathbb{R}^n$ and $\mathbb{C}^n$, Spatial Vectors

## Complex Numbers

The following are some of the standard sets of numbers.

- Natural numbers: $\mathbb{N}=\{0, 1, 2, 3, \ldots\}$ (for the purposes of this class, $0\in\mathbb{N}$)
- Integers: $\mathbb{Z}=\{\ldots, -2, -1, 0, 1, 2, \ldots\}$
- Rational numbers: $\mathbb{Q}=\{\frac{p}{q} \mid p,q \in \mathbb{Z}, q \ne 0, \text{gcd}(p,q)=1 \}$
- Real numbers: $\mathbb{R}$ is the set of all numbers on the real number line including both rational and irrational numbers.
- Complex numbers: $\mathbb{C}=\{x+iy \mid x,y \in \mathbb{R}\}$ ($x$ is called the real part and $y$ is called the imaginary part).

These standard sets can be arranged in increasing order:

$$
\mathbb{N} \subset \mathbb{Z} \subset \mathbb{Q} \subset \mathbb{R} \subset \mathbb{C}
$$

> [!def] Definition (Imaginary Unit)
> $i$ is the imaginary solution to $x^2 + 1 = 0 \implies i = \sqrt{-1}$

> [!thm] Theorem
> In $\mathbb{R}$, $\sqrt{-1}$ is not defined.

Unlike real numbers which are represented on a number line, complex numbers are represented on the complex plane where the $x$-axis is the $\mathbb{R}$ axis while the $y$-axis is the $Im$ axis. Notice the connection made with a vector $\begin{bmatrix}a \\ b\end{bmatrix}$.
![[complex_plane.png|300]]

> [!thm] Theorem
> We need complex numbers in order to be able to solve all non-constant polynomials ($p(x)=a_nx^{n}+a_{n-1}x^{n-1}+a_{n-2}x^{n-2}+\cdots+a_1x+a_0$).

> [!def] Definition (Root/Solution)
> A root/solution, $z$, of a polynomial, $p(x)$, is $z \in \mathbb{C} \st p(z) = 0$.

> [!thm] Theorem
> Every non-constant polynomial has a solution in $\mathbb{C}^n$.

> [!cor] Corollary
> In $\mathbb{C}$, every polynomial can be factored. Furthermore, by repeated factoring, we can get linear factors which are the roots of the polynomial.

### Operations with Complex Numbers

We also want to define operations between $z,z' \in \mathbb{C}$ where $z=x+iy$ and $z'=x'+iy'$. The result of these operations should also be in $\mathbb{C}$.

- **Equality of Complex Numbers:** Two complex numbers $z=x+iy$ and $z'=x'+iy'$ are considered equal if and only if $x=x'$ and $y=y'$.
- **Addition of Complex Numbers:** $z+z' = x+iy + x'+iy' = (x+x') + i(y+y') \in \mathbb{C}$
- **Multiplication of Complex Numbers:** To define multiplication, observe the identity $i^2=-1$.
  $$
      \begin{aligned}
          zz' &= (x+iy)(x'+iy') \\
          zz' &= xx' + ixy' + ix'y + i^2yy' \\
          zz' &= (xx' + (-1)yy') + i(xy'+x'y) \\
          zz' &= (xx' - yy') + i(xy'+x'y) \in \mathbb{C}
      \end{aligned}
  $$
- **Complex Conjugate:** $z=x+iy \implies \overline{z}=x-iy$
  Geometrically, this is represented by a reflection over the real axis of the complex plane
- **Inverses of Complex Numbers:** $z^{-1} = \frac{1}{z} = \frac{\overline{z}}{z\overline{z}}$

> [!thm] Theorem
> $z = x+iy \in \mathbb{R} \iff y = 0$

> [!cor] Corollary
> $z \in \mathbb{R} \iff z = \overline{z}$
>
> > _Proof._
> > First, we prove the forward implication: $z \in \mathbb{R} \implies z = \overline{z}$.
> >
> > $$
> >     \begin{gather}
> >         z=x+iy \in \mathbb{R} \implies y=0 \\
> >         z = x + 0i = x \\
> >         \overline{z} = x-iy = x - 0i = x \\
> >         \therefore z = \overline{z} \\
> >     \end{gather}
> > $$
> >
> > Secondly, we prove the converse: $z=\overline{z} \implies z \in \mathbb{R}$.
> >
> > $$
> >     \begin{gather}
> >         z = \overline{z} \implies x+iy = x-iy \\
> >         iy = -iy \\
> >         2iy = 0 \text{ (n.b., $i$ is invertible i.e., $\frac{1}{i} \in \mathbb{C}$)} \\
> >         y = 0 \\
> >         \therefore z \in \mathbb{R} \\
> >     \end{gather}
> > $$
> >
> > _QED_

> [!thm] Theorem
> The inverse of a complex number is a complex number.
>
> > _Proof._
> >
> > $$
> >    \begin{aligned}
> >        \frac{1}{z} &= \frac{\overline{z}}{z\overline{z}} \\
> >        &= \frac{(x-iy)}{(x+iy)(x-iy)} \\
> >        &= \frac{x-iy}{x^2-(iy)^2} \\
> >        &= \frac{x-iy}{x^2-i^2y^2} \\
> >        &= \frac{x-iy}{x^2-(-1)y^2} \\
> >        &= \frac{x-iy}{x^2+y^2} \\
> >        &= \frac{x}{x^2+y^2} + i\frac{-y}{x^2+y^2} \in \mathbb{C} \\
> >    \end{aligned}
> > $$
> >
> > _QED_

> [!lemma] Lemma
> $z\overline{z} = x^2+y^2 = \lvert z \rvert ^2 \in \mathbb{R}$

> [!def] Definition (Euclidean Norm/Modulus of a Complex Number)
> From the geometric representation of a complex number, we can find the magnitude of the vector using $\lvert z \rvert = \sqrt{z\overline{z}} = \sqrt{x^2+y^2}$.

> [!thm] Theorem
> $\lvert z \rvert = 0 \iff z = 0$
>
> > _Proof._
> >
> > 1. Show $\lvert z \rvert = 0 \implies z = 0$
> >    $$
> >    \begin{gather}
> >        \lvert z \rvert = 0 = \sqrt{x^2 + y^2} \\
> >        \therefore x = 0 \text{ and } y = 0 \\
> >        \therefore z = 0 + 0i = 0
> >    \end{gather}
> >    $$
> > 2. Show $z = 0 \implies \lvert z \rvert = 0$
> >    $$
> >    \begin{gather}
> >        z = 0 = x + iy \\
> >        \therefore x = 0 \text{ and } y = 0 \\
> >        \therefore \lvert z \rvert = \sqrt{0^2 + 0^2} = 0
> >    \end{gather}
> >    $$
> >
> > _QED_

## Standard Algebraic Structures

Let $M$ be a “set with multiplication” (i.e., the multiplication operation is defined on elements in the set and the product is also in that set).

> [!def] Definition (Multiplicative Inverse)
> Given $A \in M$, we say $B \in M$ is an inverse of $A$ if $AB = BA = 1$. Furthermore, we say that $A$ is invertible if $\exists B \in M$ such that $B$ is an inverse of $A$.

**Example:** The inverse of $2 \in \mathbb{R}$ is $\frac{1}{2}$. However, 2 is not invertible in $\mathbb{N}$ or $\mathbb{Z}$.

> [!lemma] Lemma
> If an inverse of $A$ exists, it is unique.

A matrix $A \in M_n(\mathbb{R})$ is invertible if $\exists B \in M_n(\mathbb{R})$ s.t. $AB = BA = I_n$.

> A note about notation: $M_n(\mathbb{R}) = \{n \times n \text{ matrix with entries in } \mathbb{R}\}$

> [!def] Definition (Ring)
> A nonempty set $R$ is a ring if addition and multiplication is defined on the set and respects the following axioms:
>
> - Associativity: $(a+b)+c = a+(b+c)$ and $(ab)c = a(bc)$
> - Zero element: $\forall a \in R,\exists 0 \in R, a+0 = 0+a = a$
> - Negative: $\forall a \in R, \exists -a \in R, a + (-a) = (-a) + a = 0$
> - Commutative addition: $\forall a, b \in R, a + b = b + a$
> - Distributive: $\forall a, b, c \in R, a(b+c) = ab + ac \land (b+c)a = ba + ca$

A ring can be called a **commutative ring** if $\forall a,b \in R, ab = ba$.

Thus, $\mathbb{Z}, \mathbb{R}, \mathbb{C}, M_n(\mathbb{R})$ are rings while $\mathbb{N}$ is not a ring.

> [!def] Definition (Field)
> A field is a commutative ring if every nonzero $a \in R$ has a multiplicative inverse (i.e., $\exists a^{-1} \in R \st aa^{-1} = a^{-1}a = 1$).

Thus, although $\mathbb{Z}$ is a ring, it is not a field because some elements do not have a multiplicative inverse (e.g., integers $\ge 2$). $\mathbb{R}, \mathbb{C}, M_n(\mathbb{R})$ are fields.

Fields are the building blocks of vector spaces. In this course, we will focus on fields $K=\mathbb{R}$ and $K=\mathbb{C}$.

**Example:** Prove that $A = \begin{pmatrix} 0 & 1 \\ 0 & 0 \end{pmatrix}$ is not invertible.

> _Proof._
> By contradiction, assume $A$ is invertible.
>
> $$
> \exists B \in M_2(\mathbb{R}) \st AB = BA = \begin{pmatrix} 1 & 0 \\ 0 & 1 \end{pmatrix} \\            \begin{pmatrix} 0 & 1 \\ 0 & 0 \end{pmatrix} \begin{pmatrix} a & b \\ c & d \end{pmatrix} = \begin{pmatrix} c & d \\ 0 & 0 \end{pmatrix} \ne \begin{pmatrix} 1 & 0 \\ 0 & 1 \end{pmatrix}
> $$
>
> Since this is a contradiction, $A$ must be not invertible.
> _QED_

## Geometry of Complex Numbers

Since complex numbers can be represented on the complex plane, we can examine the geometry of complex numbers.

**Example:** A circle of radius $r$ can be represented by the following set of points: $C = \{z \in \mathbb{C} \mid \lvert z \rvert = r\}$

If we look at the unit circle, we can notice the periodicity of $i^k$: $i^0 = 1, i^1 = i, i^2 = -1, i^3 = -i, i^4 = 1 \ldots$ The implication of this is that it is very easy to take $i$ to large exponents since $i^k = i^{k\%4}$.

### Euler’s Formula

Euler's formula established a relationship between trigonometric functions and the complex exponential function.

$$
e^{i\theta} = \cos\theta + i\sin\theta
$$

If we observe that $\lvert e^{i\theta} \rvert = \sqrt{\cos^2\theta + \sin^2\theta} = 1$, we can conclude that $e^{i\theta}$ must parametrize the unit circle.

# Chapter 2: Algebra of Matrices

A matrix $A$ over a field $K$ is a rectangular matrix of the form:

$$
A = \begin{bmatrix}
    a_{11} & a_{12} & \cdots & a_{1n} \\
    a_{21} & a_{22} & \cdots & a_{2n} \\
    \vdots & \vdots & \ddots & \vdots \\
    a_{m1} & a_{m2} & \cdots & a_{mn} \\
\end{bmatrix}
$$

The element $a_{ij}$ is the entry in row $i$ and column $j$. A matrix with $m$ rows and $n$ columns is called an $m \times n$ matrix.

## Matrix Addition and Scalar Multiplication

**Matrix Addition** is defined as follows:

$$
A + B = \begin{bmatrix}
    a_{11} + b_{11} & a_{12} + b_{12} & \cdots & a_{1n} + b_{1n} \\
    a_{21} + b_{21} & a_{22} + b_{22} & \cdots & a_{2n} + b_{2n} \\
    \vdots & \vdots & \ddots & \vdots \\
    a_{m1} + b_{m1} & a_{m2} + b_{m2} & \cdots & a_{mn} + b_{mn} \\
\end{bmatrix}
$$

**Matrix Scalar Multiplication** is defined as follows:

$$
kA = \begin{bmatrix}
    ka_{11} & ka_{12} & \cdots & ka_{1n} \\
    ka_{21} & ka_{22} & \cdots & ka_{2n} \\
    \vdots & \vdots & \ddots & \vdots \\
    ka_{m1} & ka_{m2} & \cdots & ka_{mn} \\
\end{bmatrix}
$$

## Matrix Multiplication

Matrix multiplication is defined between a matrix $A$ with dimensions $m \times p$ and a matrix $B$ with dimensions $p \times n$ whose product is a $m \times n$ matrix. Note that matrix multiplication is _not commutative_.

**Matrix multiplication** is described by the equation below where the $c_{ij}$ element is the dot product of row $i$ in $A$ and column $j$ in $B$.

$$
\begin{bmatrix}
    a_{11} & \cdots & a_{1p} \\
    \vdots & \cdots & \vdots \\
    a_{i1} & \cdots & a_{ip} \\
    \vdots & \cdots & \vdots \\
    a_{m1} & \cdots & a_{mp} \\
\end{bmatrix}
\begin{bmatrix}
    b_{11} & \cdots & b_{1j} & \cdots & b_{1n} \\
    \vdots & \cdots & \cdots & \cdots & \vdots \\
    \vdots & \cdots & \cdots & \cdots & \vdots \\
    \vdots & \cdots & \cdots & \cdots & \vdots \\
    b_{p1} & \cdots & b_{pj} & \cdots & b_{pn} \\
\end{bmatrix}
=
\begin{bmatrix}
    c_{11} & \cdots & c_{1p} \\
    \vdots & \cdots & \vdots \\
    \vdots & c_{ij} & \vdots \\
    \vdots & \cdots & \vdots \\
    c_{mn} & \cdots & c_{mn} \\
\end{bmatrix}
$$

Given square $n \times n$ matrices $A$ and $B$, each element in the product $C$ is defined by the following sum.

$$
c_{ij} = a_i \cdot b_j = \sum_{k=1}^{n}a_{ik}b_{kj}
$$

## Transpose of a Matrix

The **transpose of a matrix** $A$ denoted $A^T$ is the matrix obtained by making the columns in $A$ into rows in $A^T$.

$$
\begin{bmatrix}
    1 & 2 & 3 \\
    4 & 5 & 6
\end{bmatrix}^T=
\begin{bmatrix}
    1 & 4 \\
    2 & 5 \\
    3 & 6 \\
\end{bmatrix}
$$

## Square Matrices

**Definition (Square Matrix):** A square matrix has the same number of rows and columns (i.e., it is an $n \times n$ matrix).

**Definition (Diagonal):** The diagonal of a square matrix are the entries $a_{11}, a_{22}, \ldots, a_{nn}$.

**Definition (Trace):** The trace of $A$, denoted $\text{tr}(A)$ is the sum of the diagonal elements.

$$
\text{tr}(A) = a_{11} + a_{22} + \cdots + a_{nn}
$$

> [!def] Definition (Identity Matrix)
> The identity matrix, denoted $I_n$ is the $n$-square matrix with 1's in the diagonal and 0's everywhere else. A unique property is that for any matrix $A$, $AI = IA = A$.

# Chapter 4: Vector Spaces

In a vector space $V$ where $K$ is the field of scalars, we can define vector addition and scalar multiplication as follows:

> [!def] Definition (Vector addition and Scalar Multiplication)
>
> - **Vector addition:** For any $u, v \in V$, we assign a sum $u + v \in V$.
> - **Scalar multiplication:** For any $v \in V$ and $\lambda \in K$, we assign a product $\lambda v \in V$.

In addition to defining vector addition and scalar multiplication on a nonempty set, the following axioms must hold for arbitrary vectors $u,v,w$ in a vector space $V$.

- **Associative vector addition:** $(u + v) + w = u + (v + w)$
- **Origin/zero vector or neutral element for addition:** $u + 0 = 0 + u = u$
- **Negative:** $u + (-u) = (-u) + u = 0$
- **Commutative:** $u + v = v + u$
- **Distributive scalar:** $k(u+v) = ku + kv$ where $k \in K$
- **Distributive vector:** $(a+b)u = au + bu$ where $a,b \in K$
- **Associative scalar multiplication:** $(ab)u = a(bu)$
- **Unit scalar:** $1u=u$ where $1 \in K$

> [!def] Definition (Vector Space)
> A vector space is a nonempty set with vector addition and scalar multiplication defined and respects all of the above axioms.

## Examples of Vector Spaces

### Space $K^n$

Given an arbitrary field $K$, the $K^n$ notation is used to denote the set of all $n$-tuples of elements in $K$.

$$
K^n = \left\{\begin{bmatrix}a_1 \\ a_2 \\ \vdots \\ a_n\end{bmatrix} \mid a_i \in K \right\}
$$

In $K^n$, vector addition is defined as: $\begin{bmatrix}a_1 \\ a_2 \\ \vdots \\ a_n\end{bmatrix} + \begin{bmatrix}b_1 \\ b_2 \\ \vdots \\ b_n\end{bmatrix} = \begin{bmatrix}a_1 + b_1 \\ a_2 + b_2 \\ \vdots \\ a_n + b_n\end{bmatrix}$.

In $K^n$, scalar multiplication is defined as: $\lambda \begin{bmatrix}a_1 \\ a_2 \\ \vdots \\ a_n\end{bmatrix} = \begin{bmatrix}\lambda a_1 \\ \lambda a_2 \\ \vdots \\ \lambda a_n\end{bmatrix}$.

### Polynomial Space $P_n$

Let $P_n$ denote the set of all polynomials of the form $p(t)=a_0+a_1t+a_2x^2+\cdots+a_nx^n$ where $n \in \mathbb{N}$.

The degree of a polynomial refers to the highest power in the polynomial with a nonzero coefficient. Exceptionally, the degree of the zero polynomial is undefined.

Here, vector addition and scalar multiplication can be represented by adding polynomials and multiplying each term of the polynomial by the scalar respectively.

The standard basis of this space is: $e_0 = 1, e_1 = x, e_2 = x^2, \ldots, e_n = x^n$.

The set of all polynomials is denoted $P_\infty$. Therefore, $P_m \subseteq P_n \subsetneq P_\infty$ if $m \le n$.

Also, although polynomials have multiplication defined, they are only well defined in $P_\infty$ since $x^n \in P_n$ but $x^n \times x^n = x^2n \notin P_n$.

### Matrix Space $M_{m,n}$

$M_{m,n}$ is the set of all $m \times n$ matrices with fields in $K$. We can use the matrix addition and scalar multiplication of matrices.

Multiplication is well defined on square $n \times n$ matrices in vector space $M_n(K)$ since the product of two $n \times n$ matrices is still $n \times n$.

The standard basis of $M_2$ is:

$$
e_{11}=\begin{bmatrix}
    1 & 0 \\
    0 & 0 \\
\end{bmatrix},

e_{12}=\begin{bmatrix}
    0 & 1 \\
    0 & 0 \\
\end{bmatrix},

e_{21}=\begin{bmatrix}
    0 & 0 \\
    1 & 0 \\
\end{bmatrix},

e_{22}=\begin{bmatrix}
    0 & 0 \\
    0 & 1 \\
\end{bmatrix}
$$

### Function Space $F(D)$

$F(D)$ is the set of all functions with nonempty domain set $D$ into a field $K$. We can denote this using set builder notation as $\{f \colon D \to K\}$. This function assigns a value in $K$ $\forall x \in D$.

Vector addition of two functions $f$ and $g$ in $F(D)$ is the function $(f+g)(x) = f(x) + g(x)$.

Scalar multiplication of a function $f$ in $F(D)$ and $k$ in $K$ is the function $(kf)(x) = kf(x)$.

The zero vector in $F(D)$ is called the zero function and maps every values $x \in X$ to the zero element (i.e., $f(x) = 0, \space \forall x \in X$).

A function in $F(D)$ can be entirely defined by an $n$-tuple where $n$ is the size of $D$. For example, if $D = \{1, \ldots, n\}$, then a function $f = (f(1), \ldots, f(n))$.

**Kronecker functions** $\delta_k \colon D \to K$ indexed by $k \in D$ can be used to define the standard basis of $F(D)$.

$$
\delta_k = \begin{cases}
    1 & k = l \\
    0 & k \ne l
\end{cases}
$$

## Linear Combinations and Spanning Sets

> [!def] Definition (Linear Combination)
> A vector $v \in V$ is a linear combination of a finite set of vectors $\{u_1, u_2, \ldots, u_n\}$ if there exists $\lambda_i \in K$ which satisfy $v = \lambda_1u_1 + \lambda_2u_2 + \cdots \lambda_nu_n = \sum_{i=1}^n \lambda_iu_i$.

**Example:** Express $v = \begin{bmatrix}3 \\ 7 \\ -4\end{bmatrix} \in \mathbb{R}^3$ as a linear combination of $u_1 = \begin{bmatrix}1 \\ 2 \\ 3\end{bmatrix}, u_2 = \begin{bmatrix}2 \\ 3 \\ 7\end{bmatrix}, u_3 = \begin{bmatrix}3 \\ 5 \\ 6\end{bmatrix}$. We can solve using the following augmented matrix and row reducing.

$$
\left[
    \begin{array}{ccc|c}
        1 & 2 & 3 & 3 \\
        2 & 3 & 5 & 7 \\
        3 & 7 & 6 & -4 \\
    \end{array}
\right]
$$

After solving, we find that $v = 2u_1 - 4u_2 + 3u_3$.

What happens if we get an inconsistent solution? This would suggest that the vector is not in the span of the other vectors and it is linearly independent from them.

> [!def] Definition (Spanning Set)
> If every vector in a vector space $V$ can be expressed as a linear combination of vectors $u_1, u_2, \ldots, u_n$, then we can say $V = \text{span}(u_1, u_2, \ldots, u_n)$. In other words, the span of the set of vectors is the set of all linear combinations those vectors.
>
> $$
> \text{span}(u_1, u_2, \ldots, u_n) = \left\{\sum_{i=0}^{n} \lambda_iu_i \mid \lambda_i \in K \right\}
> $$

The canonical basis vectors can be used to span $\mathbb{R}^n$ or $\mathbb{C}^n$.

$$
e_1 = \begin{bmatrix}1 \\ 0 \\ \vdots \\ 0 \\ 0\end{bmatrix}, e_2 = \begin{bmatrix}0 \\ 1 \\ \vdots \\ 0 \\ 0\end{bmatrix}, \ldots, e_n = \begin{bmatrix}0 \\ 0 \\ \vdots \\ 0 \\ 1\end{bmatrix}
$$

The following example illustrates how spans over $\mathbb{C}$ contrasts but is connected to spans over $\mathbb{R}$.

**Example:** Let $e_1 = \begin{bmatrix}1 \\ 0\end{bmatrix}$ and $e_2 = \begin{bmatrix}0 \\ 1\end{bmatrix}$ be the standard basis of $\mathbb{C}^2$.

$$
\begin{aligned}\mathbb{C}^2 &= \text{span}_\mathbb{C}(e_1, e_2) \\
            &= \{z_1e_1 + z_2e_2 \mid z_1,z_2 \in \mathbb{C}\} \\
            &= \{(x_1 + y_1i)e_1 + (x_2 + y_2i)e_2 \mid x_1, x_2, y_1, y_2 \in \mathbb{R}\} \\
            &= \{x_1e_1 + y_1ie_1 + x_2e_2 + y_2ie_2 \mid x_1, x_2, y_1, y_2 \in \mathbb{R}\} \\
            &= \text{span}_\mathbb{R}(e_1, ie_1, e_2, ie_2)\end{aligned}
$$

This example shows that while $\mathbb{C}^2$ can be spanned by just 2 vectors, we need 4 vectors to span it over $\mathbb{R}$. It can be shown that in general $\mathbb{C}^n$ can be spanned in $n$ vectors over $\mathbb{C}$ and $2n$ vectors over $\mathbb{R}$.

**Remark.** The properties of spans over fields, like many theorems over fields, do not hold in a generalization to rings. Spans over rings are studied in a different field (pun totally intended) of study and are more involved.

## Subspaces

> [!def] Definition (Subspace)
> If $W$ is a subset of a vector space $V$ over a field $K$ and $W$ is a vector space over $K$ with respect to the vector addition and scalar multiplication operations on $V$, then $W$ is a subspace of $V$.

In order to show that a set $W$ is a vector space, normally, we would need to show that it satisfies the eight axioms outlined previously. However, if $W$ is the subset of a known vector space $V$, some of the axioms automatically hold so we do not need to reprove them.

> [!thm] Theorem (Subspace Criterion)
> If $W$ is a subset of a known vector space $V$, we can use the subspace criterion to prove that $W$ is also a vector space. $W$ is subspace of $V$ if and only if:
>
> 1. Zero vector: $0 \in W$
> 2. Closed under vector addition: $u, v \in W \implies u + v \in W$
> 3. Closed under scalar multiplication: $k \in K, u \in W \implies ku \in W$

The last two properties can be combined to form the equivalent property which says that the linear combination of $u, v \in W$ is also in $W$. This fact can be used to show that the span of some vectors in a vector space forms a subspace. Also, notice that closed under scalar multiplication implies the existence of the zero vector since we can argue that $k = 0 \implies 0u = 0 \in W$.

> [!prop] Proposition
> There are only 2 subspaces of $K$ itself (where $K=\mathbb{R}$ or $K=\mathbb{C}$).
>
> > _Proof._ The subspaces of $K$ are $\{\{0\}, K\}$.
> >
> > $\{0\}$ is always a subspace and it is the smallest subspace since all vector spaces contain the zero vector. This also means that we cannot have an empty subspace.
> >
> > $K$ is also obviously a subspace since $K \subseteq K$ and $K$ is a vector space.
> >
> > Now, we prove that there is no other subset of $K$. Let $U\subseteq K$ be a subspace of $K$. We know that $U = \{0\}$ or $U \neq \{0\}$. We need to show that if $U \neq \{0\} \implies U = K$.
> >
> > Let $u \in U$ s.t. $u \ne 0$.
> > $K = \span(u)$ which means that the other subset must be all of $K$.

Spanning sets can be shown to be subspaces. The proof by the subspace criterion is quite trivial since it is analogous to linear combinations. However, it shows that for any subspace we can find a set of vectors which spans it.

> [!prop] Proposition
> The $\span(v_1, \ldots, v_n)$ is a subspace of $V$. This can be trivially proven by the subspace criterion.

## Intersections, sums, and direct sums

Let $U$ and $V$ be subspaces in some vector space $W$ over the field $K$.

### Intersections

> [!prop] Proposition
> The intersection $U \cap V$ is a subspace.
>
> > _Proof._
> > Let $u,v \in U \cap V$, $k \in K$
> >
> > (a) Closed under vector addition
> > $u \in U$ and $u \in V$ > > $v \in U$ and $v \in V$
> >
> > Since $U$ is a subspace, $u + v \in U$.
> > Since $V$ is a subspace, $u + v \in V$
> > Therefore, $u + v \in U \cap V$.
> >
> > (b) Closed under scalar multiplication
> > $u \in U \cap V \implies u \in U \text{ and } u \in V$
> > Since $U$ and $V$ are subspaces, they are closed under scalar multiplication so $ku \in U$ and $ku \in V$.
> > Therefore, $ku \in U \cap V$.
> >
> > _QED_

### Unions

> [!prop] Proposition
> The union $U \cup V$ is a subspace if and only if $U \subseteq V$ or $V \subseteq U$.
>
> > _Proof._
> > (a) Forward implication
> > Assume $U \cup V$ is a subspace.
> > For proof by contradiction, assume $U \nsubseteq V$ and $V \nsubseteq U$.
> > $\therefore \exists u \in U \st u \notin V, v \in V \st v \notin U$.
> > $u, v \in U \cup V$
> > Since $v \notin U$ and $u \notin V$, $u + v \notin U \cup V$. This violates the subspace criterion which is a contradiction.
> >
> > (b) Converse
> > If $U \subseteq V$ then $U \cup V = V$.
> > If $V \subseteq U$ then $U \cup V = U$.
> > Since $U$ and $V$ are both subspaces, in all cases, $U \cup V$ is a subspace.
> >
> > _QED_

### Sums

> [!prop] Proposition
>
> The sum $U + V = \{u+v \mid u \in U, v \in V\}$ is a subspace.
>
> > _Proof._
> > Let $u_1, u_2 \in U$, $v_1, v_2 \in V$, and $k \in K$. Then, $u \coloneqq u_1 + v_1, v \coloneqq u_2 + v_2 \in U + V$.
> >
> > (a) Closed under vector addition
> > $u+v = (u_1 + v_1) + (u_2 + v_2) = (u_1 + u_2) + (v_1 + v_2)$ > > $u_1 + u_2 \in U$ and $v_1 + v_2 \in V$ > > $\therefore u+v \in U+V$
> >
> > (b) Closed under scalar multiplication
> > $k(u+v) = ku + kv$ > > $ku \in U$ and $kv \in V$ since $U$ and $V$ are closed under scalar multiplication.
> > $\therefore k(u+v) \in U+V$
> >
> > _QED_

> [!cor] Corollary
> $U + V = \span(U, V)$
>
> > _Proof._
> > (a) $U + V \subseteq \span(U, V)$
> > Suppose $u \in U$ and $v \in V$. Then, $u + v \in U + V$.
> > By definition, the vector $u + v$ is a linear combination of a vector from $U$ and a vector from $V$ so it is in $\span(U, V)$.
> >
> > (b) $\span(U, V) \subseteq U + V$
> > Let $w \in \span(U, V)$. By definition, $w = a_1u + a_2v$ where $a_1, a_2 \in K$, $u \in U$ and $v \in V$.
> > $a_1u \in U$ and $a_2v \in V$, so $w \in U + V$
> >
> > _QED_

### Direct Sums

> [!def] Definition (Direct Sum)
> A sum $U+V$ is a direction sum, denoted $U \oplus V$, if $U \cap V = \{0\}$.

> [!prop] Proposition
> $U$ and $V$ are in direct sum $\Leftrightarrow$ a non-zero vector $w = u + v$ has a unique decomposition (i.e., $u \in U$ and $v \in V$ are unique).
>
> > _Proof._
> > (a) Forwards implication
> > If $U$ and $V$ are in direct sum, then $U \cap V = \{0\}$.
> > Assume for contradiction $w = u_1 + v_1 = u_2 + v_2$ where $u_1 \neq u_2$ and $v_1 \neq v_2$.
> > $u_1 + v_1 = u_2 + v_2$ > > $u_1 - u_2 = v_2 - v_1$ > > $u_1 - u_2 \in U$ and $v_2 - v_1 \in V$
> > Therefore, $w \in U \cap V$ which only contains the zero vector, so $w = 0$. This is a contradiction.
> >
> > (b) Converse
> > A non-zero vector has a unique decomposition $w = u+v$ $\implies$ $U$ and $V$ are in direct sum.
> > Contrapositive: $U$ and $V$ are not in direct sum $\implies$ $w = u + v$ does not have a unique decomposition.
> > $U$ and $V$ are not in direct sum means $\exists z \in U \cap V \st u \neq 0$.
> > So, we can rewrite $w = u + v$ as $w = (u + z) + (v - z)$ and since $u+z \in U$ and $v - z \in V$, this is a different but still a valid decomposition of $w$.
> >
> > _QED_

## How to span a vector space

### Building a spanning set

We can inductively build a spanning set of a vector space. Let $W$ be a vector space over $K$.

Base case: the smallest possible vector space is $V = \{0\}$.

Inductive step: If $W = V$, halt. Otherwise, $W \neq V$ so we take a vector $w \in W \st w \notin V$. We add the span of this vector to $V$ such that $V \gets V \oplus \span(w)$.

As long as $W \neq V$, there always exists a vector $w \in W \st w \notin V$. In the end, we find vectors $w_1, \ldots, w_n \in W$ such that $W = \span(w_1, \ldots, w_n)$.

This iterative process only halts after $n$ steps if and only if the vector space is finite dimensional with dimension $n$. The choice of vector in each inductive step does not matter as long as it is not already in $V$ before it is added.

### Dimension

> [!def] Definition (Finite Dimensional Space)
> A vector space $V$ is finite dimensional if it can be spanned by finitely many vectors. (i.e., $\exists v_1, \ldots, v_n$ such that $V = \span(v_1, \ldots, v_n)$). If it cannot be spanned by finitely many vectors, it is infinite dimensional.

> [!def] Definition (Dimension)
> $\dim(V)$ is the smallest number of vectors that span $V$. Special cases, if $V = \{0\}$, then $\dim(V) = 0$ and if $V$ is infinite dimensional, then $\dim(V) = \infty$.

**Example:** $K^n$ is a finite dimensional space spanned by its standard basis vectors $\ebasis$.

**Example:** Show that $\dim(P_\infty) = \infty$.

> _Proof._
>
> For contradiction, assume $P_\infty$ is spanned by finitely many polynomials: $f_1, \ldots, f_n$. There exists one polynomial in this set that has the highest degree. Let that polynomial be $f_m$ with $deg(f_m) = M$. Then all polynomials with degree greater than $M$ cannot be in the span of these polynomials since vector addition and scalar multiplication as defined on polynomials does not increase the max degree. Therefore, $P_\infty$ must be infinite dimensional.
>
> _QED_

## Linear Independence

> [!def] Definition (Linear Independence)
> A set of vectors $v_1, \ldots, v_n \in V$ are linearly independent if
>
> $$
> k_1v_1 + \cdots + k_nv_n = 0 \implies k_1 = \cdots = k_n = 0
> $$
>
> where $k_1, \ldots, k_n \in K$.

Given vectors $v_1, \ldots, v_n$, to determine if they are linearly independent, we solve the system $k_1v_1 + \cdots + k_nv_n = 0$ (using Gaussian Elimination). If the solution set is only the zero vector, then $\begin{pmatrix}k_1 \\ \vdots \\ k_n\end{pmatrix} = \begin{pmatrix}0 \\ \vdots \\ 0\end{pmatrix} \implies k_1 = \cdots = k_n = 0$. Otherwise, there exists a non-trivial solution and the set of vectors are linearly dependent.

> [!def] Definition (Linear Dependence)
> Naturally, a set of vectors are linearly dependent if they are not linearly independent. i.e.,
>
> $$
> \exists k_1, \ldots, k_n \text{ not all zero} \st k_1v_1 + \cdots + k_nv_n = 0
> $$

> [!prop] Proposition
> A set of vectors $v_1, \ldots, v_n$ are linearly dependent if and only if there exists one of the vectors is in the span of the rest of the vectors.

> [!prop] Proposition
> If the determinant of an upper triangular matrix (computed by multiplying the entries along the diagonal) is zero if and only if the columns of the matrix are linearly dependent.

## Main Basis Theorem

> [!def] Definition (Basis)
> The basis of a vector space $V$ is a set of vectors that are linearly independent and a spanning set of $V$.

Although the notions of spanning sets and linearly independent sets doesn't really care about the ordering of the vectors, it is often helpful to express a basis as a tuple so that the basis vectors can be used for a coordinate system.

> [!lemma] Lemma
> The number of vectors in the basis of $V$ is equal to $\dim(V)$.
>
> > _Proof._
> >
> > Let $\ebasis$ be a basis of $V$.
> > Since the set is linearly independent, $n \le \dim(V)$.
> > Since the set is spanning, $n \ge \dim(V)$.
> > Putting this together, $n = \dim(V)$.
> >
> > _QED_
>
> Of course, this means that given two bases $e, f$, then $|e|=|f|$.

Often when we want to determine if a set of vectors forms a basis of a vector space, the number of vectors is equal to the dimension. If it is not, we can immediately determine that it is not due to the previous Lemma (if there are fewer vectors it won't be a spanning set while if there are more vectors it won't be a linearly independent set). However, when the number of vectors matches the dimension, it is redundant to check both spanning and linear independence, since they imply each other.

> _Proof._
>
> Given $\dim(V) = n$.
>
> If $(\ebasis)$ is spanning, then it must also be minimally spanning since any fewer vectors cannot span all of $V$.
> If $(\ebasis)$ is linearly independent, it must also be maximally linearly independent since any more vectors in $V$ will be in $\span(\ebasis)$.
> Thus, if either condition is met, then $(\ebasis)$ is a basis.

In practice, it is often easier to prove linearly independent rather than spanning.

> [!thm] Theorem (Basis Theorem)
> A basis is the minimal spanning set and the maximal linearly independent set. (n.b., although this does not imply bases are unique, it does imply that all bases of the same vector space have the same cardinality).

It is also often helpful to have a standard basis for vectors spaces. The standard bases and their respective dimensions are:

1. Space $K^n$ (dimension $n$)
   $\left\{\begin{pmatrix}1 \\ 0 \\ \vdots \\ 0 \\ 0\end{pmatrix},\begin{pmatrix}0 \\ 1 \\ \vdots \\ 0 \\ 0\end{pmatrix},\ldots,\begin{pmatrix}0 \\ 0 \\ \vdots \\ 1 \\ 0\end{pmatrix},\begin{pmatrix}0 \\ 0 \\ \vdots \\ 0 \\ 1\end{pmatrix}\right\}$
2. Space $P^n$ (dimension $n+1$)
   $(1, X, X^2, \ldots, X^n)$
3. Space $M_n(K)$ (dimension $n^2$)
   $(e_11, \ldots, e_nn)$
4. Space $F(D)$ where $D$ is finite (dimension $|D|$)
   $(\delta_x)_{x \in D}$

## Coordinates

> [!thm] Theorem (Basis-Coordinates)
> $(\ebasis)$ is a basis of $V$ if and only if every vector $v \in V$ can be expressed as a unique linear combination of the basis vectors.
>
> $$
> v = \sum{k_i e_i}
> $$
>
> > _Proof._
> >
> > (a) Forwards implication
> >
> > It is trivial to prove that $v \in V$ can be expressed as a linear combination of the basis vectors since the basis vectors are a spanning set of $V$.
> >
> > We will prove uniqueness by contradiction. Assume for contradiction, that there are two coordinates $(k_1, \ldots, k_n)$ and $(k_1', \ldots, k_n')$.
> > So, $v = \sum k_ie_i = \sum k_i'e_i$.
> > By subtraction, we find that $\sum (k_i - k_i')e_i = 0$. Since $\ebasis$ is linearly independent, $\forall i, k_i - k_i' = 0$ and so $\forall i, k_i = k_i'$. This is shows that the two coordinates must have been identical to begin with thus proving uniqueness. (Note the connection with the proof of unique decomposition for $U \oplus V$)
> >
> > (b) Converse
> >
> > Once again, it is trivial to show that if every $v \in V$ as unique coordinates, then $(\ebasis)$ is a spanning set of $V$.
> >
> > To prove linear independence, we need to show that the only solution to $k_1e_1+\cdots+k_ne_n=0$ is the trivial solution. And, indeed it is, because vectors have unique coordinates, $(k_1, \ldots, k_n) = (0, \ldots, 0)$. Thus, we have shows that $(\ebasis)$ is also linearly independent.
> >
> > Since $(\ebasis)$ is both a spanning set of $V$ and linearly independent, it is a basis of $V$.
> >
> > _QED_

**Example:** Consider the standard basis $e=(\ebasis)$ of $K^n$ and a vector $v = \veckn \in V$.

It can be expressed as the following sum:

$$
v = x_1e_1 + \cdots + x_ne_n = \sum x_ie_i
$$

To find the coordinates of a vector in an alternative basis, solve the following linear system:

$$
\left[
\begin{array}{ccc|c}
e_1 & \cdots & e_n & v
\end{array}
\right]
$$

**Example:** Let $D = \{0, 1, 2, 3, 4\}$ and consider the function space $F(D)$. Find the coordinates of $g(x)=x^2$ in the standard basis.

$$
\begin{pmatrix} g(0) \\ g(1) \\ g(2) \\ g(3) \\ g(4) \end{pmatrix}
=
\begin{pmatrix} 0 \\ 1 \\ 4 \\ 9 \\ 16 \end{pmatrix}
$$

## Finding a basis

> [!thm] Basis Algorithm
> Let $U = \span(v_1, \ldots, v_m)$. The basis algorithm can be used to get a basis of $U$.
>
> 1. Define matrix $A$ where the rows correspond to the vectors. $A = \begin{bmatrix} v_1 \\ \vdots \\ v_n \end{bmatrix}$
> 2. Use Gaussian Elimination to get Row Echelon Form, matrix $B$.
> 3. The non-zero rows in $B$ form a basis of $U$.
>
> > _Proof of correctness._
> >
> > The elementary row operations do not affect the row space of the matrix, it is an invariant (row space is the span of the row vectors).
> > When we get to an REF, the vectors are linearly independent.
> > So the non-zero vectors in $B$ are linearly independent and still form a spanning set of $U$. This is the definition of a basis of $U$.

## More Basis Theorems

> [!thm] Theorem (Transfer theorem)
> If $u_1, \ldots, u_n$ is a spanning set of $V$ and $v_1, \ldots, v_n$ is a linearly independent set in $V$, then both $u_1, \ldots, u_n$ and $V$ and $v_1, \ldots, v_n$ are bases of $V$.

> [!thm] Theorem (Basis extraction theorem)
> If $V = \span(v_1, \ldots, v_n)$, then some subset $\{e_1, \ldots, e_m\} \subseteq \{v_1, \ldots, v_n\}$ forms a basis of $V$.

> [!thm] Casting-Out Algorithm
> We can use the basis extraction theorem to actually extract a basis from a spanning set.
> Like the basis algorithm, let $U = \span(v_1, \ldots, v_m)$. The casting-out algorithm will extract a basis of $U$ from $v_1, \ldots, v_m$.
>
> 1. Define matrix $A$ where the columns correspond to the vectors. $A = \begin{bmatrix}v_1 & \cdots & v_n\end{bmatrix}$
> 2. Use Gaussian Elimination to get Row Echelon Form, matrix $B$.
> 3. The original vectors corresponding to the columns with the pivots in $B$ for a basis of $U$.
>
> > _Proof of correctness._
> > The linear dependency relations between columns are preserved by elementary operations.

> [!thm] Theorem (Basis extension theorem)
> Given a linearly independent set $e_1, \ldots, e_m$ in $V$. There exists $e_{m+1}, \ldots, e_n$ in $V$ such that $\ebasis$ forms a basis of $V$.

> [!cor] Corollary
> If $U$ is a subspace of $V$, then $\dim(U) \le \dim(V)$.

> [!cor] Corollary
> If $U$ is a subspace of $V$, there exists a subspace $U' \subseteq V$ called a supplement of $U$ such that $V = U \oplus U'$.
>
> Note that supplements are not unique. Example, in $\mathbb{R}^3$, all lines not on a plane are valid supplements. The supplement are the vectors added in the Basis Extension Theorem.

> [!thm] Theorem (Basis juxtaposition theorem)
> Suppose that $U$ and $V$ are two subspaces in direct sum (i.e., $U \cap V = \{0\}$). Let $(e_1, \ldots, e_n)$ be a basis of $U$ and $(f_1, \ldots, f_n)$ be a basis of $V$. Then, $(e_1, \ldots, e_n, f_1, \ldots, f_n)$ is a basis of $U \oplus V$.

> [!cor] Corollary
>
> $$
> \begin{gathered}
>      \dim(U + V) \le \dim(U) + \dim(V) \\
>     \dim(U + V) = \dim(U) + \dim(V) - \dim(U \cap V) \\
>     \dim(U \oplus V) = \dim(U) + \dim(V)
> \end{gathered}
> $$

# Chapter 5 and 6: Linear Mappings and Matrices

## A platter of definitions

> [!def] Definition (Transformation)
> A transformation (also referred to as map or function) $T$ takes an element $u \in U$ and assigns a value $v \in V$, denoted $v = T(u)$. The set $U$ is called the domain and the set $V$ is called the co-domain of $T$.
>
> $$
> \begin{aligned}
>     T : U \to V \\
>     u \mapsto T(u)
> \end{aligned}
> $$

> [!def] Definition (Injective)
> A transformation $T: U \to V$ is injective if $\forall u,v \in U, T(u) = T(v) \implies u = v$. The equivalent contrapositive is $\forall u,v \in U, u \ne v \implies T(u) \ne T(v)$.
>
> Connection to calculus: the horizontal line test.

> [!def] Definition (Surjective)
> A transformation $T : U \to V$ is surjective if $\forall v \in V, \exists u \in U \st v = T(u)$.
>
> Connection to calculus: range of the function is the codomain.

> [!def] Definition (Bijective)
> A transformation is bijective if it is both injective and surjective.

> [!def] Definition (Invertible)
> A transformation $T:U \to V$ is invertible if there exists $S:V \to U$ such that $ST = \Id_U$ and $TS = \Id_V$.

> [!lemma] Lemma
> Let $T:U \to V$ and $S: V \to U$ be transformations.
> $ST = \Id \implies T \text{ injective } \land S \text{ surjective }$
>
> > _Proof._
> >
> > We assume $ST = \Id$ but for contradiction assume $T$ not injective or $S$ not surjective.
> >
> > If $T$ not injective, $\exists u, v \in U$ where $u \ne v$ such that $T(u) = T(v)$.
> > Then apply $S$ to both sides to get $S(T(u)) = S(T(v))$.
> > Since $S \circ T = \Id$, $u = v$ which is a contradiction and so $T$ must be injective.
> >
> > If $S$ not surjective, $\exists u \in U$ such that $\forall v \in V, S(v) \ne u$. Let $v = T(u)$. Then $S(T(u)) = u$ which is a contraction and so $S$ must be surjective.
> >
> > Therefore, in all cases, $T$ is injective and $S$ is surjective so the original statement is proven.

> [!thm] Theorem
> A transformation $T:U \to V$ is bijective if and only if it is invertible.
>
> > _Proof._
> > We observe $ST = \Id$ implies $T$ injective and $S$ surjective
> > and that $TS = \Id$ implies $S$ injective and $T$ surjective.
> > Therefore, both $T$ and $S$ are injective and surjective so both are bijective.

%% > [!def] Definition (Image)

> The image of a transformation $T : U \to V$ is the subset of $V$ that is reachable. More formally, $\Im(T) = \{T(u) \mid u \in U\}$. %%

> [!def] Definition (Composition)
> Let $T:U \to V$ and $S:V \to W$ be transformations. Then, define $R = S \circ T$ as the composition of $T$ and $S$. To use the mapping, first apply $T$ to an element in $U$ to get an element in $V$. Then, the element in $V$ can be mapped to an element in $W$ by $S$. Overall, it can be expressed as $R(u) = S(T(u))$.

> [!def] Definition (Linear Transformation)
> A transformation is linear if
>
> 1. $T$ preserves addition: $T(u+v) = T(u) + T(v)$, and
> 2. $T$ preserves scalar multiplication $T(ku) = kT(u)$

We also not that if $T$ is linear, then $T(0) = 0$. If $T(0) \ne 0$, the transformation is called affine and not discussed in this course.

> [!def] Definition (Isomorphism)
> T is an isomorphism if it is linear and bijective.

^b7ac63

**Example:** Show that if $S$ and $T$ are linear, then $S \circ T$ is also linear.

> _Proof._
>
> (a) Preserves addition
>
> $$
> S(T(u+v)) = S(T(u) + T(v)) = S(T(u)) + S(T(v))
> $$
>
> (b) Preserves scalar multiplication
>
> $$
> S(T(ku)) = S(kT(u)) = kS(T(u))
> $$

**Example:** Show that if $S$ and $T$ are isomorphic, then $S \circ T$ is also isomorphic.

> _Proof._
>
> (a) $S \circ T$ is linear
>
> See previous example.
>
> (b) $S \circ T$ is bijective
> 
> Let $T: U \to V$ and $S: V \to W$.
> 
> Since $T$ is injective, $u \ne v \implies T(u) \ne T(v)$.
> Furthermore, since $S$ is also injective, $T(u) \ne T(v) \implies S(T(u)) \ne S(T(v))$.
> Therefore, $S \circ T$ is injective.
> 
> Since $T$ is surjective, $\forall v \in V, \exists u \in U \st T(u) = v$.
> Furthermore, since $S$ is also surjective, $\forall w \in W, \exists v \in V \st S(v) = w$. We can use the surjectivity of $T$ to argue that we can get to any $v \in V$.
> Therefore, overall $S \circ T$ is surjective.
> 
> $S \circ T$ is injective and surjective, so it is bijective.

**Example:** Prove that if $S: U \to V$ is an isomorphism and $T = S^{-1}$, then $T$ is an isomorphism.

> _Proof._
> 
> (a) Proof of bijectivity
> 
> Since $T$ is invertible, it is bijective.
> 
> (b) Proof of linearity
> 
> (b-i) Preserves addition
> Let $v_1, v_2 \in V$. Then, $\exists u_1, u_2, u_3 \in U$ such that $u_1 = T(v_1), u_2 = T(v_2), u_3 = T(v_1 + v_2)$.
> 
> Therefore, $S(u_1) = v_1, S(u_2) = v_2, S(u_3) = v_1 + v_2$.
> 
> By the linearity of $S$, $S(u_1 + u_2) = S(u_1) + S(u_2) = v_1 + v_2 = S(u_3)$.
> 
> Then, by the injectivity of $S$, $u_1 + u_2 = u_3$.
> 
> Thus, $T(v_1) + T(v_2) = u_1 + u_2 = u_3 = T(v_1 + v_2)$ and so $T$ preserves addition.
> 
> (b-ii) Preserves scalar multiplication
> Let $v \in V$. Then, $\exists u_1, u_2 \in U$ such that $u_1 = T(v)$ and $u_2 = T(kv)$.
> 
> Therefore, $S(u_1) = v$ and $S(u_2) = kv$.
> 
> By linearly of $S$, $kS(u_1) = kv \implies S(ku_1) = kv \implies S(ku_1) = S(u_2)$.
> 
> By injectivity of $S$, $ku_1 = u_2$.
> 
> Thus, $kT(v) = ku_1 = u_2 = T(kv)$ and so $T$ preserves scalar multiplication.
> 
> Therefore, $T$ is linear and so $T$ is an isomorphism.

**Example:** Show that a linear transformation is associated with a matrix.

$$
\begin{aligned}
T: K^n \to K^n \\
X \mapsto AX
\end{aligned}
$$

> _Proof of linearity._
> 
> 1. $A(X+Y) = AX + AY$
> 2. $A(kX) = k(AX)$

> [!def] Definiton (Linear Operators)
> An operator $T$ on $U$ is a linear transformation where the domain equals the codomain.
> 
> $$
> T: U \to U
> $$
> 
> The fact that $T$ is an operator means it can be iterated. We use the notation $T^k$ to denote the map $u \mapsto T(T(T\cdots(T(u))))$.
> 
> The iterated vectors $u \mapsto T(u) \mapsto T^2(u) \mapsto T^3(u) \mapsto \cdots$ form an ***orbit*** of $u$ under the transformation $T$.

## Scaling Transformation

Of course, I had to include the incredible fish scaling diagram from the notes.

![[pichot_fish.png|center|500]]

> [!def] Definition (Scaling Transformation)
> A linear transformation $T: U \to U$ is a scaling transformation if there exists a basis $(u_1, \ldots, u_n)$ of $U$ and scalars $k_1, \ldots, k_n \in K$ such that $\forall i, T(u_i) = k_iu_i$.
> 
> Note: later, we see that these transformations are formally called diagonalizable.

In scaling transformations, if $|k_i| > 1$, vectors are stretched along the corresponding basis vector and if $0 < |k_i| < 1$, the vectors are shrunk along the corresponding basis vector. If $k_i$ = 0, it is a projection and if $|k_i| = 1$ then there is no stretching. Finally, if $k_i < 0$, the vector is reflected along an axis perpendicular to the corresponding basis vector.

## Linear Map

> [!prop] Proposition
> Let $(u_1, \ldots, u_n)$ be a basis of $U$ and let $v_1, \ldots, v_n \in V$. Then, $\exists! T: U \to V \st T(u_i) = v_i$ 

**Example:** The derivative of polynomials is a linear transformation. ^dc8646
$$
\begin{aligned}
D: P_n \to P_n \\
f \mapsto f'
\end{aligned}
$$

It is defined on its standard basis as $D(X^k) = kX^{k-1}$. ^7aaf59

> _Proof of linearity._
> 
> (a) Preserves addition: $(f+g)' = f' + g'$
> (b) Preserves scalar multiplication $(kf)' = kf'$

The $\Im(D) = P_{n-1}$ so it is not surjective. However, if we redefine $D: P_\infty \to P_\infty$, the derivative is surjective. The derivative of the anti-derivative is the original function.
$D$ is also not injective since $D(0) = D(1) = 0$ but $0 \ne 1$. (This is why we need the "$+c$" when integrating)

**Example:** The integral/anti-derivative is a linear transformation.
$$
\begin{aligned}
\int: P_\infty \to P_\infty \\
f \mapsto \int f
\end{aligned}
$$

It is defined on its standard basis as $\int(X^k) = \frac{X^{k+1}}{k+1}$.

> _Proof of linearity._
> 
> (a) Preserves addition: $\int(f+g) = \int f + \int g$
> (b) Preserves scalar multiplication $\int(kf) = k \int f$

**Example:** Notice that $D \circ \int = \Id$. So, $\int$ is injective and $D$ is surjective. Note that $\int \circ D \ne \Id$.

**Remark:** The point of this section is to point out that it is only possible to have linear operators to be exclusively injective or surjective (i.e., XOR, not both) in infinite dimensional spaces such as $D$ and $\int$ which are operators on $P_\infty$.
In other words, in finite dimensional spaces, $T$ injective $\Leftrightarrow$ $T$ surjective $\Leftrightarrow$ $T$ bijective.

## Projections

> [!def] Definition (Projections)
> A projection is a scaling transformation where all basis vectors are scaled by 0 or 1.
> More formally, let $V = U \oplus V$ be a vector space with a direct sum decomposition.
> Then, every $v \in V$ has a unique decomposition $v = u + u'$.
> So, the projection onto $U$ along $U'$ is defined as
> $$
> \begin{aligned}
> P: V \to V \\
> u + u' \mapsto u
> \end{aligned}
> $$
> So, $P(v) = u = v - u'$.

> [!prop] Proposition
> Projections are linear transformation
> 
> > _Proof._
> > 
> > (a) Preserves addition
> > Let $v_1, v_2 \in V$.
> > $v_1 = u_1 + u_1'$ and $v_2 = u_2 + u_2'$.
> > $$
> > \begin{aligned}
> > P(v_1 + v_2) &= P(u_1 + u_1' + u_2 + u_2') \\
> > &= P((u_1 + u_2) + (u_1' + u_2')) \\
> > &= u_1 + u_2 \\
> > \end{aligned}
> > $$
> > Since $P(v_1) = u_1$ and $P(v_2) = u_2$, we have $P(v_1 + v_2) = P(v_1) + P(v_2)$.
> > So, projections preserve addition.
> > 
> > (b) Preserves scalar multiplication
> > Let $v \in V$ and $k \in K$.
> > Then, $v = u + u'$.
> > $$
> > \begin{aligned}
> > P(kv) &= P(k(u + u')) \\
> > &= P(ku + ku') \\
> > &= ku
> > &= P(u + u')
> > &= P(v)
> > \end{aligned}
> > $$
> > Therefore, projections preserve scalar multiplication.
> > 
> > Thus, projections are linear.
> > 
> > _QED_

## Kernel and Image

> [!def] Definition (Image)
> The image (a.k.a. range) of $T$ is a subspace of the codomain and includes all vectors that have a corresponding vector in the domain.
> $$
> \Im(T) = \{T(u) \mid u \in U\}
> $$

> [!def] Definition (Kernel)
> The kernel (a.k.a. null space) of $T$ is a subspace of the domain where $T(u) = 0$.
> $$
> \ker(T) = \{u \in U \mid T(u) = 0\}
> $$

> [!prop] Proposition
> $T$ surjective $\Leftrightarrow$ $\Im(T) = V$
> 
> _Proof._
> 
> The definition of surjectivity, $\forall v \in V, \exists u \in U \st T(u) = v$ means that every $v \in V$ is also $v \in \Im(T)$ so $V \subseteq \Im(V)$.
> By the definition of image, $\Im(T) \subseteq V$.
> So, in a surjective transformation, $\Im(T) = V$.

> [!prop] Proposition
> $T$ injective $\Leftrightarrow$ $\ker(T) = \{0\}$
> 
> _Proof._
> 
> (a) Show that $T$ injective $\implies$ $\ker(T) = \{0\}$
> 
> (a-i) Show that $\{0\} \subseteq \ker(T)$.
> 
> Trivial since kernel is a subspace.
> 
> (a-ii) Show that $\ker(T) \subseteq \{0\}$.
> 
> We know that $0 \in \ker(T)$
> For contradiction, suppose there was another nonzero vector $u \in \ker(T)$.
> This is a contradiction since $0 \ne u$ but $T(0) = T(u)$ violating injectivity.
> Therefore, $\ker(T) \subseteq \{0\}$.
> 
> (b) Show that $\ker(T) = \{0\}$ $\implies$ $T$ injective
> 
> Let $u, v \in U$ such that $T(u) = T(v)$.
> Then, $T(u-v) = T(u) - T(v) = 0$.
> Since $\ker(T) = \{0\}$, $u-v = 0 \implies u = v$.
> Therefore, $T$ is injective.

> [!prop] Proposition (Homogeneous linear systems)
> Let $A$ be a the matrix associated with the following linear transformation:
> $$
> \begin{aligned}
> T: K^n \to K^n \\
> X \mapsto AX
> \end{aligned}
> $$
> Then, $\ker(T) = \{X \in K^n \mid AX = 0\}$
> 
> In other words, finding a basis of $\ker(T)$ is algorithmically equivalent to for the solution set to a homogeneous system.

> [!prop] Proposition
> Let $U$ be a vector space with $e = (\ebasis)$ basis and $T: U \to V$ be a linear transformation. Then, the $\Im(T) = \span(T(e_1), \ldots, T(e_n))$.
> 
> > _Proof._
> > 
> > (a) $\Im(T) \subseteq \span(T(e_1), \ldots, T(e_n))$
> > Let $v \in V$. So, $\exists u \in U$ such that $T(u) = v$.
> > By taking coordinates, $u = \sum x_ie_i$.
> > Then, $v = T(u) = T\left(\sum x_ie_i\right) = \sum x_iT(e_i) \in \span(T(e_1), \ldots, T(e_n))$
> > 
> > (b) $\span(T(e_1), \ldots, T(e_n)) \subseteq \Im(T)$
> > This is more or less the reverse of part (a).

> [!thm] Theorem (Rank-nullity)
> Let $T: U \to V$ be a linear transformation from vector space $U$ to $V$, both finite dimensional. Then,
> $$
> \dim(U) = \dim(\ker(T)) + \dim(\Im(T))
> $$

## Matrix of a linear transformation

> [!prop] Proposition
> Every linear transformation $T: K^n \to K^n$ has an $n \times n$ matrix $A$ in basis $e=(\ebasis)$ where the $i$-th column of $A$ correspond to $T(e_i)$.
> $$
> A = \begin{bmatrix}
> T(e_1), \cdots, T(e_n)
> \end{bmatrix}
> $$
> 
> Furthermore, to generalize to $T: U \to V$, where $e = (\ebasis)$ is the basis for $U$ and $f = (f_1, \ldots, f_m)$ is the basis for $V$. Then, the $A$ matrix is $m$ rows and $n$ columns.
> $$
> A = \begin{bmatrix}[T(e_1)]_f, \ldots, [T(e_n)]_f\end{bmatrix}
> $$

**Example:** Determine the matrix of $D: P_n \to P_n$ in the standard basis $e = (X^n, X^{n-1}, \ldots, x, 1)$. ($D$ is the derivative defined in [[#^7aaf59|an earlier section]])

$$
A = \begin{bmatrix}
D(X^n), D(X^{n-1}), \ldots, D(X), D(1)
\end{bmatrix}
= \begin{bmatrix}
0 & 0 & \cdots & 0 & 0 \\
n & 0 & \cdots & 0 & 0 \\
0 & n-1 & \cdots & 0 & 0 \\
\vdots & \vdots &  & \vdots & \vdots \\
0 & 0 & \cdots & 1 & 0
\end{bmatrix}
$$

**Example:** Determine the matrix of a rotation $R_\theta: \mathbb{R}^2 \to \mathbb{R}^2$ in the standard basis.

$$
A = \begin{bmatrix}
R_\theta(e_1), R_\theta(e_2)
\end{bmatrix}
= \begin{bmatrix}
\cos\theta & -\sin\theta \\
\sin\theta & \cos\theta \\
\end{bmatrix}
$$

> [!def] Definition (Composition)
> If $T: U \to U$ is a linear operator, the notation $T^2, T^3, \ldots, T^n$ refers to $n$-fold composition:
> $$
> \begin{aligned}
> T^2 &= T \circ T \\
> T^3 &= T \circ T \circ T \\
> T^n &= T \circ \cdots \circ T
> \end{aligned}
> $$

## Main Isomorphism Theorem

[[#^b7ac63|The definition of isomorphism was previously introduced.]] Here we use some new notation to further explore isomorphism.

Let $\varphi: U \to V$ (phi) be an isomorphism between vector spaces $U$ and $V$.
Since $\varphi$ is bijective, then there exists an inverse $\varphi^{-1} = \psi$ where $\psi: V \to U$ (psi).
Furthermore, we say that $U$ is isomorphic to $V$, denoted $U \simeq V$, if there exists an isomorphism $\varphi: U \to V$.

> [!thm] Theorem (Main isomorphism theorem)
> Let $V$ be a vector space of dimension $n$ over a field $K$.
> $$
> V \simeq K^n
> $$

**Remark:** Isomorphisms are very handy since we can solve many problems easily in $K^n$ and generalized to other abstract vector spaces by isomorphism.

**Remark:** Furthermore, to check if two spaces over a field $K$ are isomorphic, it is sufficient to check that $\dim(U) = \dim(V)$.

> [!thm] Theorem
> Let
> $$
> \begin{aligned}
> \varphi: K^n &\to V \\
> \begin{pmatrix} k_1 \\ \vdots \\ k_n \end{pmatrix} &\mapsto \sum k_ie_i
> \end{aligned}
> $$
> 
> Then, $\varphi$ is an isomorphism if and only if $(\ebasis)$ is a basis of $V$.
> 
> > _Proof._
> > 
> > (a) $\varphi$ surjective $\Leftrightarrow$ $\span(\ebasis) = V$
> > 
> > Firstly, we show $\varphi$ surjective $\implies$ $\span(\ebasis) = V$
> > $\varphi$ surjective means that $\forall v \in V$, $v = \sum k_ie_i$. Therefore, $v \in \span(\ebasis)$ and so $V \subseteq \span(\ebasis)$.
> > Obviously, $\span(\ebasis) \subseteq V$ since the vectors are all in $V$.
> > 
> > Secondly, we show that $\span(\ebasis) = V$ $\implies$ $\varphi$ surjective.
> > Since $V = \span(\ebasis)$, $\forall v \in V$, we can express it as the sum $v = \sum k_ie_i$ for some values of $k_i$'s. Then, $v = \varphi\begin{pmatrix} k_1 \\ \vdots \\ k_n \end{pmatrix}$. Since $\forall v \in V$, we have found the corresponding $x \in K^n$ such that $\varphi(x) = v$, $\varphi$ is surjective.
> > 
> > Therefore, the surjectivity if and only if spanning.
> > 
> > Another way to prove.
> > Let $(f_1, \ldots, f_n)$ be the standard basis of $K^n$. We also observe that $\varphi(f_i) = e_i$ since that standard basis of $K_n$ is 0 in all but the $i$-th entry which picks out the $e_i$.
> > $\varphi$ surjective $\implies$ $V = \Im(\varphi) = \span(\varphi(f_1), \ldots, \varphi(f_n)) = \span(\ebasis)$.
> > 
> > (b) $\varphi$ injective $\Leftrightarrow$ $(\ebasis)$ is linearly independent.
> > 
> > $\varphi$ injective $\implies$ $\ker(\varphi) = \{0\}$.
> > So, $\varphi\begin{pmatrix} x_1 \\ \vdots \\ x_n \end{pmatrix} = 0 \implies x_1 = \cdots = x_n = 0$.
> > Therefore, $\sum x_ie_i = 0 \implies x_1 = \cdots = x_n = 0$.
> > Therefore, $(\ebasis)$ is linearly independent.

> [!prop] Proposition
> Let $\varphi: U \to V$ be an isomorphism and $u_1, \ldots, v_n$ be vectors in $U$. Then, $v_i = \varphi(u_i)$.
> 
> Intuitively, it turns out that $\varphi$ preserves linear relationships.
> 
> 1. $\span(u_1, \ldots, u_n) = U \Leftrightarrow \span(v_1, \ldots, v_n) = V$
> 2. $u_1, \ldots, u_n$ are linearly independent in $U$ $\Leftrightarrow$ $v_1, \ldots, v_n$ are linearly independent in $V$
> 3. $(u_1, \ldots, u_n)$ is a basis of $U$ $\Leftrightarrow$ $(v_1, \ldots, v_n)$ is a basis of $V$

## Rank Theorems

> [!cor] Corollary
> If $T: U \to U$ is a linear operator, the $T$ is an isomorphism, $T$ is injective and $T$ is surjective are equivalent.
> 
> It is obvious that isomorphism implies both injectivity and surjectivity by the definition of isomorphism.
> 
> > _Proof that injectivity implies surjectivity and isomorphism._
> > 
> > $T$ injective implies $\ker(T) = \{0\}$ and $\dim(\ker(T)) = 0$.
> > By rank-nullity theorem, $\dim(U) = \dim(\Im(T)) + \dim(\ker(T)) = \dim(\Im(T))$.
> > Since $\Im(T) \subseteq U$, $\Im(T) = U$.
> > Therefore, $T$ is surjective and furthermore $T$ is an isomorphism.
> 
> > _Proof that surjectivity implies injectivity and isomorphism._
> > 
> > $T$ surjective implies $\Im(T) = U$ and $\dim(\Im(T)) = \dim(U)$.
> > By rank-nullity theorem, $\dim(U) = \dim(\Im(T)) + \dim(\ker(T))$ so $\dim(\ker(T)) = 0$.
> > Therefore, $\ker(T) = \{0\}$ which implies injectivity and by extension isomorphism.

We know that $U = \ker(T) \oplus U'$ (where elements in the kernel go to zero and elements that are outside the kernel don't go to zero, there is not intersection since each value in the domain is mapped to only one element in the codomain). For the elements in $U'$, there exists an isomorphism with $\Im(T)$ (i.e., $U' \simeq \Im(T)$).

> [!def] Definition (Rank)
> Let $A$ be a matrix. The rank of $A$, denoted $\rk(A)$ is the dimension of the column space which we determine by counting the number of pivots in the REF.

> [!thm] Theorem (Rank-transpose)
> $$
> \rk(A) = \rk(A^t)
> $$
> The implication being that the column space and the row space of $A$ are equal which makes intuitive sense since they are the same pivots.

## Change of Coordinates

> [!def] Definition (Change of Basis Matrix)
> Let $V$ be a vector space.
> Then, define $e=(\ebasis)$ as a basis of $V$ and $f=(f_1,\ldots,f_k)$ be linearly independent vectors in $V$.
> $$
> Q = \begin{pmatrix} [f_1]_e, \ldots, [f_k]_e \end{pmatrix}
> $$
> $Q$ is the change of basis matrix from $f$-coordinates to $e$-coordinates.

To get an intuition for why $Q$ indeed takes $f$-coordinates to $e$-coordinates, consider the following. The vector $f_1$ in $f$-coordinates is $\begin{pmatrix}1 \\ 0 \\ \vdots \\ 0\end{pmatrix}$. Then, when we apply the change of basis matrix $Q$, we get $Q\begin{pmatrix}1 \\ 0 \\ \vdots \\ 0\end{pmatrix}$ which pulls out the first column of $Q$ which is $[f_1]_e$. That is precisely $f_1$ is $e$-coordinates. It is obvious that this pattern generalizes to $f_2, \ldots, f_k$ and in fact all vectors in $\span(f_1, \ldots, f_k)$ by linear properties.

> [!prop] Proposition
> Let $e$ and $f$ be bases of $V$. Define
> $$
> Q = \begin{pmatrix} [f_1]_e, \ldots, [f_n]_e \end{pmatrix}
> $$
> to be the change of basis matrix from $f$ to $e$-coordinates and
> $$
> P = \begin{pmatrix} [e_1]_f, \ldots, [e_n]_f \end{pmatrix}
> $$
> to be the change of basis matrix from $e$ to $f$-coordinates.
> 
> Then, $P=Q^{-1}$. Furthermore, in this case, the change of basis matrix is always invertible.
> 
> Intuitively, this makes sense. If we take a vector in $e$-coordinates, change it to $f$-coordinates and then change it back to $e$-coordinates, it would be a surprise if we didn't get back the original vector. Therefore, intuitively, $PQ=\Id$.
> 
> > _Proof._
> > 
> > Let $e$ and $f$ be a basis of $V$. We will also use an arbitrary vector $v \in V$.
> > 
> > Firstly, we can define the following isomorphism
> > $$
> > \begin{aligned}
> > \varphi: K^n &\to V\\
> > \begin{pmatrix} x_1 \\ \vdots \\ x_n \end{pmatrix} &\mapsto \sum x_ie_i
> > \end{aligned}
> > $$
> > By definition, the coordinates of $v$ in the $e$-coordinates is $[v]_e = \varphi^{-1}(v)$.
> > 
> > Secondly, we can define another isomorphism
> > $$
> > \begin{aligned}
> > \psi: K^n &\to V\\
> > \begin{pmatrix} y_1 \\ \vdots \\ y_n \end{pmatrix} &\mapsto \sum y_if_i
> > \end{aligned}
> > $$
> > Again, by definition, the coordinates of $v$ in the $f$-coordinates is $[v]_f = \psi^{-1}(v)$
> > 
> > Therefore, the change of basis matrix from $f$-coordinates to $e$-coordinates, $Q$, can be decomposed as going from $f$-coordinates to $V$ using $\psi$ and then going from $V$ to $e$-coordinates using $\varphi^{-1}$.
> > $$
> > Q = \varphi^{-1} \circ \psi
> > $$
> > We can do a similar decomposition of change of basis from $e$ to $f$-coordinates, $P$.
> > $$
> > P = \psi^{-1} \circ \varphi
> > $$
> > 
> > To show $P = Q^{-1}$
> > $$
> > \begin{aligned}
> > P &= Q^{-1} \\
> > \psi^{-1} \circ \varphi &= (\varphi^{-1} \circ \psi)^{-1} \\
> > \psi^{-1} \circ \varphi &=  \psi^{-1} \circ (\varphi^{-1})^{-1} \\
> > \psi^{-1} \circ \varphi &=  \psi^{-1} \circ \varphi \\
> > \end{aligned}
> > $$

**Remark (review from Math 133):** Finding the inverse of matrices may be helpful. To do so, augment the matrix with the identity matrix and use Gaussian elimination to get RREF on the left side. The right side will be the inverse.

> [!def] Definition (Change of Basis for a Linear Transformation)
> Let $T: U \to V$ be linear. Then, $[T]_{e,f}$ is the matrix of $T$ where $e$ is the basis of $U$ and $f$ is the basis of $V$.
> $$
> [T]_{e,f} = \begin{pmatrix} [T(e_1)]_f, \ldots, [T(e_n)]_f \end{pmatrix} = \psi^{-1} \circ T \circ \varphi
> $$
> 
> If we instead use a linear operator with both $e$ and $f$ being a basis of $V$, then we get
> $$
> \begin{aligned}[]
> [T]_e = \varphi^{-1} \circ T \circ \varphi \\
> [T]_f = \psi^{-1} \circ T \circ \psi
> \end{aligned}
> $$

> [!thm] Theorem (Change of basis formula)
> Let $T: V \to V$, $A = [T]_e$ and $B = [T]_f$. $e$ and $f$ are both bases of $V$. Let $Q$ denote the change of basis matrix from $f$ to $e$ coordinates and $P$ denote the change of basis matrix from $e$ to $f$ coordinates. These are related by the following formula:
> $$
> B = PAQ
> $$
> 
> > _Proof._
> > 
> > Since $A = \varphi^{-1} \circ T \circ \varphi$ and $B = \psi^{-1} \circ T \circ \psi$, we can rearrange and get $T = \varphi \circ A \circ \varphi^{-1} = \psi \circ B \circ \psi^{-1}$
> > 
> > $$
> > \begin{aligned}
> > \psi \circ B \circ \psi^{-1} &= \varphi \circ A \circ \varphi^{-1} \\
> > B &= \psi^{-1} \circ \varphi \circ A \circ \varphi^{-1} \circ \psi \\
> > B &= PAQ
> > \end{aligned}
> > $$

 This formula can be deduced intuitively. On the left-hand-side, $B$ takes vectors $v \in V$ in $f$-coordinates, and applies $T$ to them to get $T(v)$ in $f$-coordinates. On the right-hand-side, $Q$ takes vectors $v \in V$ from $f$-coordinates to $e$-coordinates. Then, $A$ takes vectors from $e$-coordinates to $T(v)$ in $e$-coordinates. Finally, $P$ takes $T(v)$ from $e$-coordinates back to $f$-coordinates. Overall, both sides achieve the same end goal, getting $T(v)$ in $f$ coordinates. The only difference being that the left-hand-side does it directly while the right-hand-side does it indirectly via $e$-coordinates.

# Chapter 9: Diagonalization, Eigenvalues and Eigenvectors

## Eigenvalues and Eigenvectors

> [!def] Definition (Eigenvalue)
> Let $T: V \to V$ be a linear transformation. A scalar $\lambda \in K$ is an eigenvalue of $T$ if $\exists v \in V \setminus \{0\}$ such that $T(v) = \lambda v$.
> 
> The equation $T(v) = \lambda v$ means that $v$ and $T(v)$ are collinear and $T$ scales $v$ by $\lambda$.

> [!def] Definition (Eigenvector)
> Given an eigenvalue $\lambda \in K$ for a linear transformation $T: V \to V$, a vector $v \in V$ such that $T(v) = \lambda v$ is called an eigenvector.

> [!def] Definition (Eigenspace)
> Let $\lambda \in K$. The set of eigenvectors for a given $\lambda$ is called the eigenspace of $T$ corresponding to the eigenvalue $\lambda$.
> $$
> E_\lambda = \{v \in V \mid T(v) = \lambda v\}
> $$

> [!prop] Proposition
> Given $\lambda \in K$.
> If $\lambda$ is not an eigenvalue, then $E_\lambda = \{0\}$.
> If $v \in E_\lambda$, then $\span(v) \subseteq E_\lambda$.

> [!prop] Proposition
> The eigenspace is a subspace of $V$.

> [!prop] Proposition
> If $\lambda_1 \ne \lambda_2$, then $E_{\lambda_1} \cap E_{\lambda_2} = \{0\}$.
> 
> > _Proof._
> > 
> > Let $v \in E_{\lambda_1} \cap E_{\lambda_2}$.
> > Since $v \in E_{\lambda_1}$, $T(v) = \lambda_1 v$.
> > Since $v \in E_{\lambda_2}$, $T(v) = \lambda_2 v$.
> > 
> > Therefore, $\lambda_1 v = \lambda_2 v \implies (\lambda_1 - \lambda_2)v = 0$. Since $\lambda_1 \ne \lambda_2$, $\lambda_1 - \lambda_2 \ne 0$ and so $v = 0$.
> > 
> > Thus, $E_{\lambda_1} \cap E_{\lambda_2} \subseteq \{0\}$ and the inclusion going the other way is obvious since the intersection of subspaces is a subspace so it must contain the zero vector.
> > 
> > _QED_

> [!cor] Corollary
> $T$ is scaling if and only if $\exists$ basis of eigenvectors if and only if $\exists \lambda_1, \ldots, \lambda_n \in K$ such that $V = E_{\lambda_1} \oplus \cdots \oplus E_{\lambda_n}$.

Two examples of linear transformation that are not scaling are rotations and shearing. For rotations, the exceptions are when $\theta = 0$ or $\theta = \pi$ which for shear with matrix $\begin{bmatrix}1 & 1 \\ 1 & 1\end{bmatrix}$, the vectors on the $x$-axis are scaled by $1$ (i.e., unaffected) while other vectors are not scaling.

> [!prop] Proposition
> Let $A \in M_n(K)$ be the matrix associated with a linear transformation $T$.
> $$
> \begin{aligned}
> T: K^n &\to K^n \\
> X &\mapsto AX
> \end{aligned}
> $$
> Then, $T$ is a scaling transformation $\Leftrightarrow$ $A$ is diagonalizable.
> 
> > _Proof._
> > Let $f=(v_1, \ldots, v_n)$ be a basis of eigenvectors of $T$ (i.e., the $v_i$'s are eigenvectors and they form a basis). This is possible by definition of scaling.
> > To write $[T]_f$, we compute $T(v_i)$ in the $f$-coordinates. Since $T(v_i) = \lambda_i v_i$ (and using the $f$-coordinates of the $v_i$ where all zero except the corresponding entry with one), we get
> > $$
> > [T]_f = \begin{bmatrix}
> > \lambda_1 & & & \\
> > & \lambda_2 & & \\
> > & & \ddots & \\
> > & & & \lambda_n \\
> > \end{bmatrix}
> > $$
> > Let $D = [T]_f$ be the diagonal matrix. Then, we use the change of basis matrices between $e$ and $f$-coordinates for $Q$ and $Q^{-1}$.
> > Then overall, we get $A = Q^{-1}DQ$.
> > %% TODO: Proof for converse %%

## How to diagonalize

Let $A \in M_n(K)$. The following 2 steps are required to diagonalize.

1. Find the eigenvalues $\lambda$ of $A$.
2. Find the eigenspaces for each $\lambda$.
$$
E_\lambda = \ker(A-\lambda\Id)
$$
which can be solved by the homogeneous system $(A-\lambda\Id)X = 0$.

> [!def] Definition (Geometric multiplicity)
> Let $A \in M_n(K)$. Then
> $$
> n_\lambda = \dim(E_\lambda)
> $$
> is the geometric multiplicity of eigenvalue $\lambda$ for matrix $A$.

> [!prop] Proposition
> $A \in M_n(K)$ is diagonalizable $\Leftrightarrow$ $n = \sum n_\lambda$

> [!cor] Corollary
> If we have $n$ distinct eigenvalues, then $A$ is always diagonalizable since the minimum $\dim(E_\lambda)$ is 1 for any eigenvalue.

> [!thm] Theorem
> The eigenvalues of $A$ are the roots of the characteristic polynomial
> $$
> \chi(\lambda) = \det(\lambda\Id - A)
> $$
> If we use polynomial factorization to get the following form
> $$
> \chi(\lambda) = (\lambda-\lambda_1)^{m_1}\cdots(\lambda-\lambda_r)^{m_r}
> $$
> where the eigenvalues $e_i$'s are pairwise distinct, then $A$ is diagonalizable.

**Remark:** By the fundamental theorem of algebra, we know that it is always possible to factorize over the field $\mathbb{C}$ but not always over the field $\mathbb{R}$ the implication that over $\mathbb{R}$ only some matrices are diagonalizable while over $\mathbb{C}$ all matrices are diagonalizable.

**Summary:** Given $A = QDQ^{-1}$, find $Q$ and $D$. To find $D$, we first find all of the eigenvalues

## Polynomials of matrices

> [!def] Definition (Powers of Matrices)
> Let $A \in M_n(K)$.
> $$
> A^n = A \ldots A \text{ ($n$ times)}
> $$

> [!thm] Theorem
> Suppose $A = QDQ^{-1}$ is diagonalizable. Then, $A^n$ is also diagonalizable.
> $$
> A^n = (QDQ^{-1})\cdots(QDQ^{-1}) = QD^nQ^{-1}
> $$

The powers of diagonal matrices are easy to compute since each entry is just raised to the $n$-th power (this is not true in general, only diagonal matrices).

$$
\begin{bmatrix}
\lambda_1 & & & \\
& \lambda_2 & & \\
& & \ddots & \\
& & & \lambda_k \\
\end{bmatrix}^n
= \begin{bmatrix}
\lambda_1^n & & & \\
& \lambda_2^n & & \\
& & \ddots & \\
& & & \lambda_k^n \\
\end{bmatrix}
$$

This can be generalized to apply to polynomials of matrices.

$$
f(A) = a_nA^n + \cdots + a_1A + a_0\Id
$$

Then, for the diagonal matrix, just apply $f$ to each entry (once again this is specific to diagonal matrices and not broadly applicable to general matrices).

$$
f\left(\begin{bmatrix}
\lambda_1 & & & \\
& \lambda_2 & & \\
& & \ddots & \\
& & & \lambda_k \\
\end{bmatrix}\right)
= \begin{bmatrix}
f(\lambda_1) & & & \\
& f(\lambda_2) & & \\
& & \ddots & \\
& & & f(\lambda_k) \\
\end{bmatrix}
$$

> [!thm] Theorem (Cayley-Hamilton)
> For an arbitrary square matrix $A$, the characteristic polynomial $\chi$ satisfies $\chi(A) = 0$.

# Chapter 7: Inner Product Spaces and Orthogonality
