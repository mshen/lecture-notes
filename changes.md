# Changes

## `quartz.config.js`
- Switch from Katex to Mathjax for better Latex support
- Add `Plugin.HardLineBreaks()` transformer to have parity with Obsidian
- Edit `quartz/styles/custom.scss`

## `package.json`
- Add `npm run build` command to `npx quartz build --output ~/public_html/lecture-notes`